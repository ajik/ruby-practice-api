class Api::V1::UsersController < ActionController::API
    def index
        limit = params[:limit]
        page = params[:page]
        @user = User.new
        if page > 0 && limit > 0
            offset = limit.to_i * (page.to_i - 1)
            @users = User.all().limit(limit).offset(offset)
        else
            @users = User.all()
        end
        
        render json: resp(200, "success", @users) 
    end

    def show
        id = params[:id]
        @user = User.find(params[:id])
        render json: @user, status: :ok
    rescue ActiveRecord::RecordNotFound => e
        render json: {
            error: e.to_s
        }, status: :not_found    
    end

    def create
        @user = User.create(user_params)
        puts @user.username
        render json: @user
    end

    def destroy
        begin
            @user = User.find(params[:id])
            @user.destroy
            render json: {
                id: @user.id
            }, status: :ok
        rescue ActiveRecord::RecordNotFound => e
            render json: {
                error: e.to_s
            }, status: :not_found
        end
    end

    def update
        User.transaction do
            @user = User.find(params[:id])
            if !(@user.username?)
                @user.update!(user_params)
                render json: @user, status: :ok
            else 
                @user.update!(user_params)
                raise ActiveRecord::Rollback, "Id not found!"
                #print "some words"
                #render json: respError(400, "error" ,"Id not found!"), status: :bad_request
            end
        rescue ActiveRecord::Rollback => e
            render json: respError(400, "error" ,e), status: :bad_request
        end
    end

    private

    def user_params
        params.require(:user).permit(:username, :emailAddress, :password)
    end

    def resp(code, status, data)
        @r = {
            :code => code,
            :status => status,
            :data => data
        }
        return @r
    end

    def respError(code, status, message)
        @r = {
            :code => code,
            :status => status,
            :message => message
        }
        return @r
    end
end