class User < ApplicationRecord
    has_many :user
    validates :username, presence: true
end
