class ChangeUsernamesToUsers < ActiveRecord::Migration[6.1]
  def change
    rename_table :usernames, :users
  end
end
