class CreateUsernames < ActiveRecord::Migration[6.1]
  def change
    create_table :usernames do |t|
      t.string :username
      t.string :password
      t.string :emailAddress

      t.timestamps
    end
  end
end
